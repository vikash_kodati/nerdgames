<!--  This jsp allows the admin to own a game. Restrictions that existed for voting
do not exist here -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Own the Game</title>
</head>
<body>

<h2>Games Under Vote</h2>

<s:if test="hasActionErrors()">
   <div class="errors">
      <s:actionerror/>
   </div>
</s:if>

<table border=1>
	<tr><td colspan="5"><h3>Title</h3></td><td><h3>Votes</h3></td></tr>
<s:iterator value="gamesUnderVote" status="state" var="g">

  <tr>
  	<s:if test="#state.even == true">
      <td colspan="5" style="background: #CCCCCC"><s:property value="title"/></td>
      <td style="background: #CCCCCC"><s:property value="votes"/></td>
    </s:if>
    <s:else>
      <td colspan="5"><s:property value="title" /></td>
      <td><s:property value="votes" /></td>
    </s:else>
    <!--  Column for owning -->  
    <td>    
    <s:form action="admin/own">
	<s:hidden id="%{#g.getId()}" key="gameOwned" label="Own Game" value="%{#g.getId()}" />	
	<s:submit value="Own it"/>
	</s:form>
    </td> 
  </tr>
</s:iterator>
</table>

<h2>Games owned by Nerdery</h2>
<table border=1>
<s:iterator value="gamesOwned" status="state">
  <tr>
  	<s:if test="#state.even == true">
      <td colspan="7" style="background: #CCCCCC"><s:property value="title"/></td>
    </s:if>
    <s:else>
      <td colspan="7"><s:property value="title"/></td>
    </s:else>
  </tr>
</s:iterator>
</table>

</body>
</html>