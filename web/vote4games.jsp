<!--  This is the jsp that handles the bulk of the requirements. 
This jsp displays the games under vote, owned games and ability to vote and propose games. -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Vote for your Game</title>
</head>
<body>

<!-- <h3>Simple Iterator</h3>
<ol>
<s:iterator value="dummyValues">
  <li><s:property /></li>
</s:iterator>
</ol>
-->


<!--  Table showing the games that are being voted
TODO: Need to display the error message when the vote should not be allowed -->
<h2>Games under Vote</h2>

<s:if test="hasActionErrors()">
   <div class="errors">
      <s:actionerror/>
   </div>
</s:if>


<!-- one row for letting the user add a new game -->
 <s:form action="addGame">
	<s:textfield label="Add Game" key="newGame" />
	<s:submit/>
</s:form>


<table border=1>
	<tr><td colspan="5"><h3> Title</h3></td><td><h3>Votes</h3></td></tr>
<s:iterator value="gamesUnderVote" status="state" var="g">
  <tr>
  	<s:if test="#state.even == true">
      <td colspan="5" style="background: #CCCCCC"><s:property value="title"/></td>
      <td style="background: #CCCCCC"><s:property value="votes"/></td>
    </s:if>
    <s:else>
      <td colspan="5"><s:property value="title" /></td>
      <td><s:property value="votes" /></td>
    </s:else>
    <!--  Column for voting -->  
    <td>    
    <!-- One design option is to show the vote button a below and display the error msg
    when the user is not allowed to vote. The other option was to not display the button itself.
    which is more about displaying only the eligible options. -->
    <s:form action="vote4game">
    
	<s:hidden id="%{#g.getId()}" key="votedGame.id" label="Vote for game" value="%{#g.getId()}" />	
	<s:submit value="Vote"/>
	</s:form>
    </td> 
    <!-- Another method to submit via button.
    TODO: yet to figure out how to show the error message
    <td>
    <s:form action="vote4game" method="post">
    <s:submit type="button"  id="%{#g.getId()}" key="votedGame.id" value="%{#g.getId()}" label="Vote2" />
    </s:form>
    </td>
    -->
  </tr>
</s:iterator>
</table>

<hr>

<h2>Games owned by Nerdery</h2>
<table border=1>

<s:iterator value="gamesOwned" status="state">
  <tr>
  	<s:if test="#state.even == true">
      <td colspan="7" style="background: #CCCCCC" ><s:property value="title"/></td>
    </s:if>
    <s:else>
      <td colspan="7" ><s:property value="title"/></td>
    </s:else>

  </tr>
</s:iterator>
</table>

</body>
</html>