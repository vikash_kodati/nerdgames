package com.kodati.nerd.model;

import java.sql.Date;
/**
 * A POJO for hand shake between the jsp and the action class.
 * @author Vikash Kodati
 *
 */
public class Game {

	private String id;
	private String title;
	private String votes;
	private boolean owned = false;
	private Date created;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public boolean isOwned() {
		return owned;
	}
	public void setOwned(boolean owned) {
		this.owned = owned;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getVotes() {
		return votes;
	}
	public void setVotes(String votes) {
		this.votes = votes;
	}

}
