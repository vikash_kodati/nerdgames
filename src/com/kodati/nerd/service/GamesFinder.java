package com.kodati.nerd.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;

import com.kodati.nerd.model.Game;

/**
 * Business Service class (analogous to session beans) that handles the details
 * of all the actions supported by the application. Currently this is tightly
 * integrated with Action Classes, however should be loosely coupled using the
 * factory pattern.
 * 
 * @author Vikash Kodati.
 * 
 */
// This class handles the database interaction although ideally should use
// entity beans for such actions.
public class GamesFinder {

	/* String constants used for reading DB info */
	private static final String JDBC_DRIVER = "JDBC_DRIVER";
	private static final String JDBC_URL = "JDBC_URL";
	private static final String DB_NAME = "DB_NAME";
	private static final String DB_USERNAME = "DB_USERNAME";
	private static final String DB_PASSWORD = "DB_PASSWORD";

	/*
	 * ResouceBundle for reading DB properties. Changing the password does not
	 * require recompile
	 */
	private static ResourceBundle s_dbProps;
	private static String s_jdbc_conn_url;
	static {
		s_dbProps = ResourceBundle.getBundle("db");
		s_jdbc_conn_url = s_dbProps.getString(JDBC_URL) + "/" + s_dbProps.getString(DB_NAME);
	}

	/**
	 * Find the games that are being actively voted. in other words games that
	 * are not yet owned
	 * 
	 * @return List of 'Game' objects.
	 */
	public List<Game> findGamesUnderVote() {
		// System.out.println("Finding games under vote");
		// String query = "select * from games where owned=0";
		String query = "select g.id, g.title, count(v.userid) as votes from games g, votes v where g.owned = 0 and g.id=v.gameid group by g.id order by votes desc, title asc";
		return executeQuery(query);
	}

	/**
	 * Finds Games that are already owned by Nerdery
	 * 
	 * @return List of 'Game' objects.
	 */
	public List<Game> findGamesOwned() {
		// System.out.println("Finding games Owned");
		String query = "select * from games where owned=1 order by title asc";
		return executeQuery(query);
	}

	/**
	 * Adds a given Game (name) to the database. First add the game to the games
	 * table, gets hold of the ID of the newly created game and then uses that
	 * create an entry in the vote table
	 * 
	 * @param newGame
	 *            The game title to add
	 * @param userId
	 *            The user who is proposing the game.
	 * @return
	 */
	public void addGame(String newGame, String userId) {
		try {
			Class.forName(s_dbProps.getString(JDBC_DRIVER));
			Connection conn = (Connection) DriverManager.getConnection(s_jdbc_conn_url, s_dbProps.getString(DB_USERNAME),
					s_dbProps.getString(DB_PASSWORD));
			String updateQuery = "insert into games (title) values ( ?) ";
			PreparedStatement stmt = conn.prepareStatement(updateQuery);
			stmt.setString(1, newGame);
			stmt.execute();
			conn.close();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::addGame: Class not found Exception: " + e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::addGame: SQL Exception: " + e.getMessage());
		}

		String newGameId = null;
		try {
			Class.forName(s_dbProps.getString(JDBC_DRIVER));
			Connection conn = (Connection) DriverManager.getConnection(s_jdbc_conn_url, s_dbProps.getString(DB_USERNAME),
					s_dbProps.getString(DB_PASSWORD));
			PreparedStatement stmt = conn.prepareStatement("select id from games where title = '" + newGame + "'");
			ResultSet results = stmt.executeQuery();

			while (results.next()) {
				// System.out.println("ID of the newly added game: " +
				// results.getString(1));
				newGameId = results.getString(1);
			}
			conn.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::addGame finding ID: Class not found Exception: " + e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::addGame finding ID: SQL Exception: " + e.getMessage());
		}

		try {
			Class.forName(s_dbProps.getString(JDBC_DRIVER));
			Connection conn = (Connection) DriverManager.getConnection(s_jdbc_conn_url, s_dbProps.getString(DB_USERNAME),
					s_dbProps.getString(DB_PASSWORD));
			String updateQuery = "insert into votes (gameid, userid) values ( ?, ?) ";
			PreparedStatement stmt = conn.prepareStatement(updateQuery);
			stmt.setString(1, newGameId);
			stmt.setString(2, userId);
			stmt.execute();
			conn.close();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::addGame updating vote table: Class not found Exception: " + e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::addGame updating vote table: SQL Exception: " + e.getMessage());
		}
	}

	/*
	 * Executes the query and returns a list of 'Game' objects Takes the query
	 * string to execute, as input.
	 */
	private List<Game> executeQuery(String query) {
		List<Game> allGames = new ArrayList<Game>();
		Game g;
		try {
			Class.forName(s_dbProps.getString(JDBC_DRIVER));
			Connection conn = (Connection) DriverManager.getConnection(s_jdbc_conn_url, s_dbProps.getString(DB_USERNAME),
					s_dbProps.getString(DB_PASSWORD));
			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet results = stmt.executeQuery();

			while (results.next()) {
				g = new Game();
				g.setId("" + results.getString(1));
				g.setTitle(results.getString(2));
				g.setVotes("" + results.getString(3));
				allGames.add(g);
			}
			conn.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::executeQuery: Class not found Exception: " + e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::executeQuery: SQL Exception: " + e.getMessage());
		}
		return allGames;
	}

	/**
	 * This method updates the database with a vote for a given game.
	 * 
	 * @param id
	 *            The ID of the game
	 * @param userId
	 *            The user ID voting. Currently the IP address of the remote
	 *            machine.
	 */
	public void castVote(String id, String userId) {
		try {
			Class.forName(s_dbProps.getString(JDBC_DRIVER));
			Connection conn = (Connection) DriverManager.getConnection(s_jdbc_conn_url, s_dbProps.getString(DB_USERNAME),
					s_dbProps.getString(DB_PASSWORD));
			String updateQuery = "insert into votes (gameid, userid) values ( ?, ?) ";
			PreparedStatement stmt = conn.prepareStatement(updateQuery);
			stmt.setString(1, id);
			stmt.setString(2, userId);
			stmt.execute();
			conn.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::castVote: Class not found Exception: " + e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::castVote: SQL Exception while casting vote: " + e.getMessage());
		}
	}

	/**
	 * This method determines whether or not the new game being proposed is a
	 * duplicate. It connects to the database and looks up for matching titles.
	 * 
	 * @param newGame
	 *            The game being proposed
	 * @return true if duplicate, else false
	 */
	public boolean isGameDuplicate(String newGame) {
		try {
			Class.forName(s_dbProps.getString(JDBC_DRIVER));
			Connection conn = (Connection) DriverManager.getConnection(s_jdbc_conn_url, s_dbProps.getString(DB_USERNAME),
					s_dbProps.getString(DB_PASSWORD));
			PreparedStatement stmt = conn.prepareStatement("Select * from games where title = '" + newGame + "'");

			ResultSet results = stmt.executeQuery();
			// System.out.println("before the duplicate iteration");
			while (results.next()) {
				System.out.println(results.getString(1) + " " + results.getString(2));
				return true;
			}
			conn.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::isGameDup: Class not found Exception: " + e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::isGameDup: SQL Exception: " + e.getMessage());
		}
		return false;

	}

	/**
	 * This method determines whether or not the current instant falls on a
	 * weekend
	 * 
	 * @return true if weekend, else false
	 */
	public boolean isWeekend() {
		Calendar date = Calendar.getInstance();
		// if (date.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
		if (date.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || date.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			return true;
		}
		// System.out.println("Its not a weekend");
		return false;
	}

	/**
	 * Given a gameID, this method updates the game in the database as owned.
	 * 
	 * @param gameId
	 *            The ID of the Game to be owned
	 * @return
	 */
	public void ownGame(String gameId) {
		System.out.println("Owning the game: " + gameId);
		try {
			Class.forName(s_dbProps.getString(JDBC_DRIVER));
			Connection conn = (Connection) DriverManager.getConnection(s_jdbc_conn_url, s_dbProps.getString(DB_USERNAME),
					s_dbProps.getString(DB_PASSWORD));
			String updateQuery = "update games set owned = ? where id = ?";
			PreparedStatement stmt = conn.prepareStatement(updateQuery);
			stmt.setString(1, "" + 1);
			stmt.setString(2, gameId);
			stmt.executeUpdate();
			//Ideally should be closed in finally block. Chose to be simple for NAT
			// as those condition will not occur here.
			conn.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			//Should use a logger (log4j) in real world applications.
			System.out.println("GamesFinder::ownGame: Class not found Exception: " + e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("GamesFinder::ownGame: SQL Exception: " + e.getMessage());
		}
	}
}
