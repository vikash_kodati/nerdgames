package com.kodati.nerd.action;

import java.util.List;

import com.kodati.nerd.model.Game;
import com.kodati.nerd.service.GamesFinder;
import com.opensymphony.xwork2.Action;

/**
 * A simple Action class that assists in owning a game. There are no validation
 * rules around this action. Uses the business service class GamesFinder to
 * actually perform the action of owning.
 * 
 * @author Vikash Kodati
 * 
 */
public class GameOwnAction implements Action {

	private String gameOwned;
	private List<Game> gamesUnderVote;
	private List<Game> gamesOwned;

	public List<Game> getGamesOwned() {
		return gamesOwned;
	}

	public void setGamesOwned(List<Game> gamesOwned) {
		this.gamesOwned = gamesOwned;
	}

	public List<Game> getGamesUnderVote() {
		return gamesUnderVote;
	}

	public void setGamesUnderVote(List<Game> dummyValues) {
		this.gamesUnderVote = dummyValues;
	}

	public String execute() {
		System.out.println("ID to be owned: " + gameOwned);
		try {
			GamesFinder gf = new GamesFinder();
			gf.ownGame(gameOwned);
			displayGames();
		} catch (Exception e) {
			return ERROR;
		}
		return SUCCESS;
	}

	public void displayGames() {
		// GamesFinder business service should be invoked using a factory
		// patterns in real world.
		// Used a simplistic approach for NAT.
		GamesFinder gf = new GamesFinder();
		gamesUnderVote = gf.findGamesUnderVote();
		gamesOwned = gf.findGamesOwned();
	}

	public String getGameOwned() {
		return gameOwned;
	}

	public void setGameOwned(String gameOwned) {
		this.gameOwned = gameOwned;
	}

}
