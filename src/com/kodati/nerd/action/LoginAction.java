package com.kodati.nerd.action;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.kodati.nerd.model.User;
import com.opensymphony.xwork2.ActionSupport;
/** Not being used for NAT */
// extending ActionSupport interface for static strings and validate method.
public class LoginAction extends ActionSupport {

	private User user;

	private List<String> games;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<String> getGames() {
		return games;
	}

	public void setGames(List<String> games) {
		this.games = games;
	}

	public String execute() {
		if ((user.getUserId() != null) && (user.getPassword() != null)) {
			if (user.getUserId().equals("nerd")
					&& user.getPassword().equals("password")) {
				return SUCCESS;
			}
		}
		return LOGIN;
	}

	public void validate() {
		if (StringUtils.isEmpty(user.getUserId())) {
			// User ID is NULL or blank.
			addFieldError("userId", "User ID cannot be blank");
		}
		if (StringUtils.isEmpty(user.getPassword())) {
			// Password is NULL or blank.
			addFieldError("password", "Password cannot be blank");
		}

	}

}
