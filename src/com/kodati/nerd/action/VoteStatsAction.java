package com.kodati.nerd.action;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.kodati.nerd.model.Game;
import com.kodati.nerd.service.GamesFinder;
import com.opensymphony.xwork2.ActionSupport;

/**
 * An action class extending from ActionSupport base class that allows the user
 * to add games and displays the owned and games under vote.
 * <ul>
 * <li>1. Does not allow adding games on weekends
 * <li>2. Does not allow to add more than once on a weekday
 * <li>3. Does not allow a duplicate game to be proposed
 * <li>4. Does not allow an empty title to be proposed
 * </ul>
 * 
 * @author Vikash Kodati
 * 
 */
@SuppressWarnings("serial")
public class VoteStatsAction extends ActionSupport implements ServletResponseAware, ServletRequestAware {

	private String newGame;
	protected HttpServletResponse servletResponse;
	protected HttpServletRequest servletRequest;
	private List<Game> gamesUnderVote;
	private List<Game> gamesOwned;

	private static String COOKIE_NEWGAME_NAME = "Hungry_Nerd_Games_Propose";

	public List<Game> getGamesOwned() {
		return gamesOwned;
	}

	public void setGamesOwned(List<Game> gamesOwned) {
		this.gamesOwned = gamesOwned;
	}

	public List<Game> getGamesUnderVote() {
		return gamesUnderVote;
	}

	public void setGamesUnderVote(List<Game> dummyValues) {
		this.gamesUnderVote = dummyValues;
	}

	public String getNewGame() {
		return newGame;
	}

	public void setNewGame(String newGame) {
		this.newGame = newGame;
	}

	public String displayGames() {
		try {
			GamesFinder gf = new GamesFinder();
			gamesUnderVote = gf.findGamesUnderVote();
			gamesOwned = gf.findGamesOwned();
			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public String addGame() {
		try {
			GamesFinder gf = new GamesFinder();
			if (gf.isWeekend()) {
				// addFieldError("newGame",
				// "New game proposal not allowed on weekends");
				addActionError("New game proposals are not allowed on weekends");
			} else if (StringUtils.isEmpty(newGame)) {
				// addFieldError("newGame", "Title cannot be empty");
				addActionError("Title cannot be empty");
			} else if (gf.isGameDuplicate(newGame)) {
				// addFieldError("newGame",
				// "We already have it proposed or owned");
				addActionError("We already have this game proposed or owned");
			} else if (!isEligibleToPropose()) {
				// addFieldError("newGame",
				// "You already proposed a new game for the day");
				addActionError("We already have your proposal for the day. Please suggest again tomorrow ");
			} else {
				gf.addGame(newGame, servletRequest.getRemoteAddr());
			}
			displayGames();
		} catch (Exception e) {
			return ERROR;
		}
		return SUCCESS;
	}

	/*
	 * Determines whether or not the user (analogous to cookie in this
	 * context)to propose by looking for the presence of a cookie true if it
	 * does not find the cookie, false if it finds. Upon false, it also inserts
	 * a new cookie by setting the appropriate expiry time.
	 */

	private boolean isEligibleToPropose() {
		// Load from cookie

		for (Cookie c : servletRequest.getCookies()) {
			if (c.getName().equals(COOKIE_NEWGAME_NAME)) {
				// cookie did not expire implying the user is not yet eligible
				// to propose new games yet.
				System.out.println("COOKIE FOUND: " + c.getName() + "  " + c.getDomain());
				return false;
			}
		}

		// he is eligible now but set the cookie too.
		// Inserted the last vote date.
		TimeZone tz = TimeZone.getTimeZone("CST");
		Cookie cook = new Cookie(COOKIE_NEWGAME_NAME, new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance(tz).getTime()));
		Cookie cook2 = new Cookie(VoteAction.COOKIE_NAME,
				new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance(tz).getTime()));

		long currentTime = Calendar.getInstance(tz).getTime().getTime();
		Calendar calendar = Calendar.getInstance(tz);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DATE);
		calendar.set(year, month, day, 23, 59, 59);
		long endOfDayTime = calendar.getTime().getTime();
		long secsLeft = endOfDayTime - currentTime;
		// System.out.println("Num of secs left for the day: " + secsLeft);
		// note: this method takes in secs
		// Need to add both cookies at the time of proposing a new game since
		// new game implies a vote for the day as well.
		cook.setMaxAge((int) secsLeft / 1000);
		cook2.setMaxAge((int) secsLeft / 1000);
		servletResponse.addCookie(cook);
		servletResponse.addCookie(cook2);
		return true;
	}

	public HttpServletResponse getServletResponse() {
		return servletResponse;
	}

	public void setServletResponse(HttpServletResponse servletResponse) {
		this.servletResponse = servletResponse;
	}

	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest servletRequest) {
		this.servletRequest = servletRequest;
	}
}
