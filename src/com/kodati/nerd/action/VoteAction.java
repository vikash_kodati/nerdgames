package com.kodati.nerd.action;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.kodati.nerd.model.Game;
import com.kodati.nerd.service.GamesFinder;
import com.opensymphony.xwork2.ActionSupport;

/**
 * An Action class that supports the voting for a game and the validations that
 * go with it. 
 * <ul>
 * <li> 1. Does not allow to vote on weekends 
 * <li> 2. Does not allow to vote more than once on a weekday
 * Once the validations are enforced, displays the games being voted and owned.
 * 
 * Uses GamesFinder service class to perform all the business logic and database
 * interaction.
 * 
 * @author Vikash Kodati
 * 
 */
@SuppressWarnings("serial")
public class VoteAction extends ActionSupport implements ServletResponseAware, ServletRequestAware {

	private Game votedGame;
	private List<Game> gamesUnderVote;
	private List<Game> gamesOwned;
	protected HttpServletResponse servletResponse;
	protected HttpServletRequest servletRequest;

	public static String COOKIE_NAME = "Hungry_Nerd_Games_Vote";

	public String execute() {
		try {
			GamesFinder gf = new GamesFinder();
			if (gf.isWeekend()) {
				addActionError("Voting not permitted during weekend");
			} else if (!isEligibleToVote()) {
				addActionError("We have accounted your vote for the day. Please vote again tomorrow");
			} else {
				gf.castVote(votedGame.getId(), servletRequest.getRemoteAddr());
			}
			displayGames();
		} catch (Exception e) {
			return ERROR;
		}
		return SUCCESS;
	}

	/*
	 * Determines whether or not the user (analogous to cookie in this
	 * context)to vote by looking for the presence of a cookie true if it does
	 * not find the cookie, false if it finds. Upon false, it also inserts a new
	 * cookie by setting the appropriate expiry time.
	 */
	private boolean isEligibleToVote() {
		for (Cookie c : servletRequest.getCookies()) {
			if (c.getName().equals(COOKIE_NAME)) {
				// cookie did not expire implying the user is not yet eligible
				// to vote.
				return false;
			}
		}

		// he is eligible now but set the cookie too.
		// Inserted the last vote date.
		TimeZone tz = TimeZone.getTimeZone("CST");
		Cookie cook = new Cookie(COOKIE_NAME, new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance(tz).getTime()));

		
		long currentTime = Calendar.getInstance(tz).getTime().getTime();
		Calendar calendar = Calendar.getInstance(tz);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DATE);
		calendar.set(year, month, day, 23, 59, 59);
		long endOfDayTime = calendar.getTime().getTime();
		long secsLeft = endOfDayTime - currentTime;
		// System.out.println("Num of secs left for the day: " + secsLeft);
		cook.setMaxAge((int) secsLeft/1000); // note: this method takes secs
		servletResponse.addCookie(cook);
		return true;
	}

	/**
	 * This method fetches the games using the service classes and sets the
	 * appropriate variables such that struts2 can displays them in the UI.
	 * 
	 * @return code indicating success vs. failure
	 */
	public void displayGames() {
		GamesFinder gf = new GamesFinder();
		gamesUnderVote = gf.findGamesUnderVote();
		gamesOwned = gf.findGamesOwned();
	}

	public Game getVotedGame() {
		return votedGame;
	}

	public void setVotedGame(Game votedGame) {
		this.votedGame = votedGame;
	}

	public HttpServletResponse getServletResponse() {
		return servletResponse;
	}

	public void setServletResponse(HttpServletResponse servletResponse) {
		this.servletResponse = servletResponse;
	}

	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest servletRequest) {
		this.servletRequest = servletRequest;
	}

	public List<Game> getGamesUnderVote() {
		return gamesUnderVote;
	}

	public void setGamesUnderVote(List<Game> gamesUnderVote) {
		this.gamesUnderVote = gamesUnderVote;
	}

	public List<Game> getGamesOwned() {
		return gamesOwned;
	}

	public void setGamesOwned(List<Game> gamesOwned) {
		this.gamesOwned = gamesOwned;
	}
}
